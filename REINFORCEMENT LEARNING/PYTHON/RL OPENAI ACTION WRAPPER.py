#!/usr/bin/env python
# coding: utf-8

# In[1]:


import gym
from typing import TypeVar
import random


# In[2]:


Action = TypeVar('Action')


# In[37]:


class RandomActionWrapper(gym.ActionWrapper):
    def __init__(self, env, epsilon = 0.7):
        super(RandomActionWrapper, self).__init__(env)
        self.epsilon = epsilon
    
    def action(self, action: Action) -> Action:
        if random.random() < self.epsilon:
            print('RANDOM!!')
            return self.env.action_space.sample()
        return action


# In[38]:


env = RandomActionWrapper(gym.make('CartPole-v0'))

obs = env.reset()
total_reward = 0.0


# In[39]:


while True:
    obs, reward, done, extra = env.step(0)
    total_reward += reward
    if done:
        break


# In[21]:


print(total_reward)


# In[ ]:




