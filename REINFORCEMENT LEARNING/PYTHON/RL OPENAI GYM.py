#!/usr/bin/env python
# coding: utf-8

# In[1]:


import gym


# In[2]:


env = gym.make('CartPole-v0')


# In[3]:


env.render


# In[4]:


obs = env.reset()


# In[5]:


obs


# In[6]:


obs = env.reset()


# In[7]:


obs


# In[8]:


env.action_space


# In[12]:


env.observation_space


# In[13]:


env.step(0)


# In[14]:


env.action_space.sample()


# In[15]:


env.action_space.sample()


# In[17]:


env.observation_space.sample()


# In[ ]:




