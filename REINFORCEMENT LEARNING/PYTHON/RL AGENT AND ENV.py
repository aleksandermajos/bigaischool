#!/usr/bin/env python
# coding: utf-8

# In[39]:


from typing import List
import random


# In[52]:


class Environment:
    def __init__(self):
        self.steps_left = 10

    def get_observation(self) -> List[float]:
        return [0.0, 0.0, 0.0]

    def get_actions(self) -> List[int]:
        return [0,1]

    def is_done(self) -> bool:
        return self.steps_left ==0

    def action(self, action: int) -> float:
        if self.is_done():
            raise Exception('Game is over')
        self.steps_left -= 1
        return random.random()


# In[53]:


class Agent:
    def __init__(self):
        self.total_reward = 0.0

    def step(self, env: Environment):
        current_obs = env.get_observation()
        actions = env.get_actions()
        reward = env.action(random.choice(actions))
        self.total_reward += reward


# In[54]:


env = Environment()
agent = Agent()


# In[55]:


while not env.is_done():
    agent.step(env)


# In[56]:


print('Total reward got: %.4f' % agent.total_reward)


# In[ ]:




