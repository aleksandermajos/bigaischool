#!/usr/bin/env python
# coding: utf-8

# In[1]:


import random


# In[17]:


arms = 7
bandits = 7
lr = 0.1
gamma = 0.9
episodes = 10000
reward = []


# In[18]:


for i in range(bandits):
    reward.append([])
    for j in range(arms):
        reward[i].append(random.uniform(-1,1))
reward


# In[19]:


def greedy(values):
    return values.index(max(values))


# In[20]:


Q =[]
for i in range(bandits):
    Q.append([])
    for j in range(arms):
        Q[i].append(10.0)
Q


# In[21]:


def learn(state, action, reward, next_state):
    q = gamma * max(Q[next_state])
    q = 0
    q += reward
    q -= Q[state][action]
    q *= lr
    q += Q[state][action]
    Q[state][action] = q


# In[22]:


bandit = random.randint(0,bandits-1)
for i in range(0, episodes):
    last_bandit = bandit
    bandit = random.randint(0,bandits-1)
    action = greedy(Q[bandit]) 
    r = reward[last_bandit][action]
    learn(last_bandit, action, r, bandit)


# In[23]:


Q


# In[ ]:




