#!/usr/bin/env python
# coding: utf-8

# In[4]:


import random


# In[15]:


reward = [1.0, 0.9, 0.2, 0.5, 0.6, 0.1, -0.6]
arms = len(reward)
lr = .1
episodes = 10000
Value = [5.0]*arms
Value


# In[16]:


def greedy(values):
    return values.index(max(values))


# In[17]:


for i in range(0, episodes):
    action = greedy(Value)
    Value[action] = Value[action]+lr*(reward[action]-Value[action])
Value


# In[ ]:




