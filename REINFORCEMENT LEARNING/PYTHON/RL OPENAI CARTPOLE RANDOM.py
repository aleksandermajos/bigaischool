#!/usr/bin/env python
# coding: utf-8

# In[1]:


import gym


# In[2]:


env = gym.make('CartPole-v0')


# In[8]:


total_reward = 0.0
total_steps = 0
obs = env.reset()


# In[13]:


env.action_space.sample()


# In[12]:


env.observation_space.sample()


# In[9]:


while True:
    action = env.action_space.sample()
    env.render()
    obs, reward, done, extra = env.step(action)
    total_reward += reward
    total_steps += 1
    if done:
        break

print('Episode done in %d steps, total reward %.2f' % (total_steps, total_reward))


# In[ ]:




