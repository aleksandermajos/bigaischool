#!/usr/bin/env python
# coding: utf-8

# In[1]:


import gym


# In[2]:


env = gym.make('CartPole-v0')
env = gym.wrappers.Monitor(env, 'recordings')


# In[3]:


total_reward = 0.0
total_steps = 0
obs = env.reset()


# In[7]:


while True:
    action = env.action_space.sample()
    obs, reward, done, extra = env.step(action)
    total_reward += reward
    total_steps += 1
    if done:
        break

print('Episode done in %d steps, total reward %.2f' % (total_steps, total_reward))


# In[ ]:





# In[ ]:




