from setuptools import setup, find_packages
from setuptools.command.build_py import build_py as DistutilsBuild
import os

setup(name='bigaischool',
      version='0.0.2',
      url='https://github.com/aleksandermajos/BIGAISCHOOL',
      license='AGPLv3',
      author='Aleksander Majos',
      author_email='aleksander.majos@gmail.com',
      packages=find_packages(),
      include_package_data=True,
      install_requires=['gym']
      )