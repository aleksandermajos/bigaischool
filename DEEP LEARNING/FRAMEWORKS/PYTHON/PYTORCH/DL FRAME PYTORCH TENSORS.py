#!/usr/bin/env python
# coding: utf-8

# # PYTORCH TENSORS
# 

# ## CREATING OF TENSORS
# 

# In[1]:


import torch
import numpy as np


# In[2]:


a = torch.FloatTensor(3,2)


# In[3]:


a


# In[4]:


a.zero_()


# In[5]:


b=torch.FloatTensor([[1,2,3],[3,2,1]])


# In[6]:


b


# In[7]:


n=np.zeros(shape=(3,2))


# In[8]:


n


# In[9]:


c = torch.tensor(n)


# In[10]:


c


# In[11]:


n=np.zeros(shape=(3,2), dtype=np.float16)


# In[12]:


c


# In[13]:


c=torch.tensor(n)


# In[14]:


c


# ## Scalar tensor

# In[15]:


a= torch.tensor([1,2,3])


# In[16]:


a


# In[17]:


s = a.sum()


# In[18]:


s


# In[19]:


s.item()


# In[20]:


b=torch.tensor(2)


# In[21]:


b.item()


# ## GPU tensors

# In[22]:


a = torch.FloatTensor([2,3])


# In[23]:


a


# In[24]:


ca = a.cuda()


# In[25]:


ca


# In[26]:


a+1


# In[27]:


ca


# In[28]:


ca+1


# In[29]:


ca.device


# In[ ]:




