#!/usr/bin/env python
# coding: utf-8

# # PYTORCH GRADIENTS

# In[104]:


import torch


# In[105]:


v1= torch.tensor([2.0, 30.0], requires_grad = True)


# In[106]:


v1.is_leaf


# In[107]:


v1.requires_grad


# In[108]:


v2= torch.tensor([1.0, 10.0], requires_grad = True)


# In[109]:


v2.is_leaf


# In[110]:


v2.requires_grad


# In[111]:


v_sum = v1+v2


# In[112]:


v_sum


# In[113]:


v_sum.is_leaf


# In[114]:


v_sum.requires_grad


# In[115]:


v_res = (v_sum*2).sum()


# In[116]:


v_res


# In[117]:


v_res.is_leaf


# In[118]:


v_res.requires_grad


# ## NOW IS THE MAGIC...CALCULATE GRADIENTS OF OUR GRAPH

# In[119]:


v_res.backward()


# In[120]:


v1.grad


# In[121]:


v2.grad


# In[122]:


print(v_sum.grad)


# In[123]:


print(v_res.grad)


# In[ ]:




