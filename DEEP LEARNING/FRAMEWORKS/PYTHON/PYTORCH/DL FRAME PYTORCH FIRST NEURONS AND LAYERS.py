#!/usr/bin/env python
# coding: utf-8

# # PYTORCH FIRST NEURONS AND LAYERS PYTORCH

# In[43]:


import torch.nn as nn
from torch import optim
import os


# In[2]:


l = nn.Linear(2,5)


# In[3]:


l


# In[4]:


l.state_dict()


# In[5]:


v = torch.FloatTensor([1,1])


# In[6]:


v


# In[7]:


v.requires_grad


# In[8]:


l(v)


# In[9]:


l.parameters()


# In[10]:


l.load_state_dict


# In[11]:


s = nn.Sequential(
    nn.Linear(2,5),nn.ReLU(),
    nn.Linear(5,20),nn.ReLU(),
    nn.Linear(20,10),nn.Dropout(p=0.3),nn.Softmax(dim=1)
    )


# In[12]:


s


# In[13]:


s.state_dict


# In[14]:


s(torch.FloatTensor([[1,2]]))


# In[15]:


class OurModule(nn.Module):
    def __init__(self, num_inputs, num_classes, dropout_prob=0.3):
        super(OurModule, self).__init__()
        self.pipe = nn.Sequential(
            nn.Linear(num_inputs,5), nn.ReLU(),
            nn.Linear(5,20), nn.ReLU(),
            nn.Linear(20, num_classes),
            nn.Dropout(p=dropout_prob), nn.Softmax()
        )
    def forward(self, x):
        return self.pipe(x)


# In[28]:


net1 = OurModule(num_inputs=2,num_classes=10)


# In[29]:


out = net1(torch.FloatTensor([[1,2]]))


# In[30]:


out


# In[31]:


net1


# In[32]:


# Initialize optimizer
optimizer = optim.SGD(net1.parameters(), lr=0.001, momentum=0.9)


# In[34]:


# Print model's state_dict
print("Model's state_dict:")
for param_tensor in net1.state_dict():
    print(param_tensor, "\t", net1.state_dict()[param_tensor].size())


# In[35]:


# Print optimizer's state_dic
print("Optimizer's state_dict:")
for var_name in optimizer.state_dict():
    print(var_name, "\t", optimizer.state_dict()[var_name])

