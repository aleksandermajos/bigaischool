#!/usr/bin/env python
# coding: utf-8

# In[1]:


import torch
print(torch.cuda.is_available())
print(torch.rand(2,2))


# In[2]:


x = torch.tensor([[0,0,1],[1,1,1],[0,0,0]])


# In[3]:


x


# In[4]:


x[0][0]=5


# In[5]:


x


# In[6]:


y= torch.zeros(2,2)


# In[7]:


y


# In[11]:


z = torch.ones(1,2)+torch.ones(1,2)


# In[12]:


z


# In[14]:


torch.rand(1).item()


# In[15]:


z.device


# In[21]:


z_gpu = z.to("cuda:1")


# In[22]:


z_gpu


# In[24]:


torch.rand(2,2).max().item()


# In[25]:


z_gpu.log2_()


# In[27]:


flat_tensor = torch.rand(784)
viewed_tensor = flat_tensor.view(1,28,28)


# In[28]:


viewed_tensor.shape


# In[29]:


torch.Size([1,28,28])


# In[30]:


res_tensor = flat_tensor.reshape(1,28,28)


# In[31]:


res_tensor.shape


# In[32]:


hwc_tensor = torch.rand(640, 480, 3)


# In[33]:


chw_tensor = hwc_tensor.permute(2,0,1)


# In[34]:


chw_tensor.shape


# In[ ]:




