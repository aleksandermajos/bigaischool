#!/usr/bin/env python
# coding: utf-8

# In[1]:


import torch
from torch import nn
from torch import Tensor
from torch import optim
import torch.nn.functional as F
import pandas as pd
import numpy as np
import matplotlib as plt
import os
import plotly.graph_objects as go
get_ipython().run_line_magic('pylab', 'inline')


# In[2]:


if not os.path.exists("Images"):
    os.mkdir("Images")


# In[3]:


df = pd.read_csv("./Data/EURUSD_H12_2002-2020.csv")


# In[4]:


df


# In[5]:


def get_last_n_candles_as_fig(df,n):
    layout = go.Layout(margin=go.layout.Margin(l=0,r=0,b=0,t=0,),xaxis = go.XAxis(
        showticklabels=False, showgrid=False,zeroline=False),yaxis = go.YAxis(showticklabels=False,showgrid=False,zeroline=False))
    fig = go.Figure(layout=layout,data=[go.Candlestick(open=df['O'][-n:], high=df['H'][-n:],
                low=df['L'][-n:], close=df['C'][-n:])
                     ])
    fig.update_layout(xaxis_rangeslider_visible=False)
    return fig


# In[6]:


fig = get_last_n_candles_as_fig(df,3)


# In[7]:


type(fig)


# In[8]:


fig.write_image("Images/UP.png")


# In[9]:


fig.write_image("Images/UP.jpeg")


# In[10]:


fig.show()


# In[ ]:




