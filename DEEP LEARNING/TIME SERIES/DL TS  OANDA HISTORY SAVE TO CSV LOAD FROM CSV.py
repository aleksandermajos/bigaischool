#!/usr/bin/env python
# coding: utf-8

# # OANDA V20 API
# # HISTORY DOWNLOADS AND SAVE AS CSV,LOAD TO PANDAS

# In[1]:


from Utils.Credentials_Utils import exampleAuth


# In[3]:


from Utils.Data_Utils import get_history_oanda, Add_Diff_CO_Column, Add_Growing_Column


# In[4]:


import pandas as pd


# In[7]:


accountID, token = exampleAuth("/home/aleksander/Oanda/")


# In[9]:


data = get_history_oanda(accountID, token, "2000-01-01T00:00:00Z", "2020-02-14T00:00:00Z", "EUR_USD", "H12", "DF")

data.head()


# In[9]:


data.to_csv("./Data/EURUSDM1_2002-2020.csv",index=0)


# In[7]:


dataLoad=pd.read_csv("./Data/EURUSDM15_2002-2019.csv")


# In[8]:


dataLoad.head()


# In[ ]:




