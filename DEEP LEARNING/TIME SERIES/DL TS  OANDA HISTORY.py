#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from Utils.Credentials_Utils import exampleAuth
from Utils.Data_Utils import get_history_oanda, Add_Diff_CO_Column, Add_Growing_Column
accountID, token = exampleAuth("/home/aleksander/Oanda/")
data = get_history_oanda(accountID, token, "2000-01-01T00:00:00Z", "2020-02-14T00:00:00Z", "EUR_USD", "H12", "DF")
data = Add_Diff_CO_Column(data)
data = Add_Growing_Column(data)

