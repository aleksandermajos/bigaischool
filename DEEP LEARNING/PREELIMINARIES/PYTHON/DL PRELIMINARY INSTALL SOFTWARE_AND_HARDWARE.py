#!/usr/bin/env python
# coding: utf-8

# # INSTALL SOFTWARE AND HARDWARE
# 
# 
# * [Anaconda v3.7](https://www.anaconda.com/distribution/#download-section) Scientific Python Distribution
#     * [Scikit-Learn](http://scikit-learn.org/)
#     * [Pandas](http://pandas.pydata.org/)
# * [Jupyter Notebooks](http://jupyter.readthedocs.io/en/latest/install.html)
# * [PyCharm IDE](https://www.jetbrains.com/pycharm/)
# * [MatPlotLib](http://matplotlib.org/)

# In[ ]:


get_ipython().system('pip install numpy')
get_ipython().system('pip install matplotlib')
get_ipython().system('pip install Pandas')
get_ipython().system('pip install Scikit-Learn')
get_ipython().system('pip install gym')


# # IF YOU HAVE CUDA DEVICE (NVIDIA GPU)
# 
# * [Nvidia Drivers](https://www.nvidia.com/Download/index.aspx?lang=en-us)
# * [Nvidia Cuda 10](https://developer.nvidia.com/cuda-downloads)
# * [Nvidia CUDNN](https://developer.nvidia.com/cudnn)
# and...

# In[ ]:


get_ipython().system('pip install tensorflow-gpu==2.0.0-alpha0')
get_ipython().system('pip install https://download.pytorch.org/whl/cu100/torch-1.0.1-cp37-cp37m-win_amd64.whl')


# # HOWEVER IF YOU DO NOT HAVE CUDA DEVICE (NVIDIA GPU)
# 
# Than...

# In[ ]:


get_ipython().system('pip install tensorflow')
get_ipython().system('pip install https://download.pytorch.org/whl/cpu/torch-1.0.1-cp37-cp37m-win_amd64.whl')
get_ipython().system('pip install torchvision')


# # CHECK EVERY PIECE THAT IS WORKING OR NOT :

# In[5]:


import torch
import tensorflow as tf
import matplotlib
import pandas, numpy
import sklearn


# In[ ]:




