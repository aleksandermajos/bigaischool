#!/usr/bin/env python
# coding: utf-8

# In[7]:


import requests
from bs4 import BeautifulSoup


# In[13]:


req = requests.get("https://context.reverso.net/translation/polish-english/tu+teraz", headers={'User-Agent': 'Mozilla/5.0'})


# In[14]:


soup = BeautifulSoup(req.text, 'html.parser')


# In[15]:


sentences = [x.text.strip() for x in soup.find_all('span', {'class':'text'}) if '\n' in x.text]


# In[16]:


sentences


# In[ ]:





# In[ ]:




