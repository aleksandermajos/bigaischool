#!/usr/bin/env python
# coding: utf-8

# In[48]:


import torch
waveglow = torch.hub.load('nvidia/DeepLearningExamples:torchhub', 'nvidia_waveglow')


# In[49]:


get_ipython().system('pip install numpy scipy librosa unidecode inflect librosa')


# In[50]:


import numpy as np
from scipy.io.wavfile import write


# In[51]:


waveglow = waveglow.remove_weightnorm(waveglow)
waveglow = waveglow.to('cuda')
waveglow.eval()


# In[52]:


tacotron2 = torch.hub.load('nvidia/DeepLearningExamples:torchhub', 'nvidia_tacotron2')
tacotron2 = tacotron2.to('cuda')
tacotron2.eval()


# In[53]:


text = "Man, if my grandfather could be here now..."


# In[54]:


# preprocessing
sequence = np.array(tacotron2.text_to_sequence(text, ['english_cleaners']))[None, :]
sequence = torch.from_numpy(sequence).to(device='cuda', dtype=torch.int64)


# In[55]:


# run the models
with torch.no_grad():
    _, mel, _, _ = tacotron2.infer(sequence)
    audio = waveglow.infer(mel)
audio_numpy = audio[0].data.cpu().numpy()
rate = 22050


# In[56]:


write("audio.wav", rate, audio_numpy)


# In[57]:


from IPython.display import Audio
Audio(audio_numpy, rate=rate)


# In[ ]:




